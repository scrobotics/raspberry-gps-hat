# Raspberry Pi GPS + BME280 HAT

This repository contains the hardware design files for a Raspberry Pi HAT that includes:
* 1x BME280
* 2x LEDs
* 1x 4-pin JST connector for UART based GPS

## License
Copyright Sparks, Circuits and Robotics SL.

This documentation describes Open Hardware and is licensed under the CERN OHL v. 1.2.

You may redistribute and modify this documentation under the terms of the CERN OHL v.1.2. (http://ohwr.org/cernohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN OHL v.1.2 for applicable conditions.